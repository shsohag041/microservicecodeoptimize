﻿using Play.Catalog.Service.Dtos;
using Play.Catalog.Service.Entities;
using System;
using System.Runtime.CompilerServices;
using System.Xml.Linq;

namespace Play.Catalog.Service
{
    public static class Extensions
    {
        public static ItemDto AsDto( this Item item)
        {
            return new ItemDto(item.Id,item.Name,item.Description, item.Title, item.Price);
           
        }

    }
}
