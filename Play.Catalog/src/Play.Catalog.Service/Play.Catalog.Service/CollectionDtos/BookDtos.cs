﻿using System;

namespace Play.Catalog.Service.CollectionDtos
{
    public class BookDtos
    {
        public record BookDto(Guid Id, string Name, string Description);
        public record CreateBoookDto(string Name, string Description);
        public record UpdateBookDto(string Name, string Description);
    }
}
