﻿using Microsoft.AspNetCore.Mvc;
using static Play.Catalog.Service.CollectionDtos.BookDtos;
using System.Collections.Generic;
using System;
using Play.Catalog.Service.CollectionDtos;

namespace Play.Catalog.Service.Controllers
{
    public class BooksController : Controller
    {
        private static readonly List<BookDto> books = new List<BookDto>
        {
            new BookDto(Guid.NewGuid(),"Hasan","seven"),
             new BookDto(Guid.NewGuid(),"Abu","eight"),
              new BookDto(Guid.NewGuid(),"Sayem","seven"),
              new BookDto(Guid.NewGuid(),"Ahmmed","nine"),
              new BookDto(Guid.NewGuid(),"Shariful","seven"),
              new BookDto(Guid.NewGuid(),"Sohag","eight"),
              new BookDto(Guid.NewGuid(),"Lima","seven"),
        };
        public IActionResult Index()
        {
            return View();
        }
    }
}
