﻿using Microsoft.AspNetCore.Mvc;
using Play.Catalog.Service.BasicRepoApi2.Entities;
using Play.Catalog.Service.BasicRepoApi2.Mapping;
using Play.Catalog.Service.BasicRepoApi2.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Play.Catalog.Service.BasicRepoApi2.Dtos.BookDtos;

namespace Play.Catalog.Service.BasicRepoApi2.Controllers
{
    [Route("Api2/[controller]")]
    [ApiController]
    public class BooksController : Controller
    {
        public readonly BooksRepository booksRepository = new();

        [HttpGet]
        public async Task<IEnumerable<BookDto>> GetBookAsync()
        {
            var books = (await booksRepository.GetBooksAsync()).Select(book => book.AsDto());
            return books;
        }
        [HttpGet("id")]
        public async Task<ActionResult<BookDto>> GetByIdAsync(Guid id)
        {
            var book = await booksRepository.GetAsync(id);
            if (book == null)
            {
                return NotFound();
            }
            //return Ok(book);
            return book.AsDto();
        }
        [HttpPost]
        public async Task<ActionResult<BookDto>> PostAsync(CreateBookDto createBookDto)
        {
            var book = new Book()
            {
                //Id = Guid.NewGuid(),
                Uid= createBookDto.Uid,
                Name = createBookDto.Name,
                Description = createBookDto.Description,
                Author = createBookDto.Author,
                Student_uid = createBookDto.Student_uid,
                Teacher_uid= createBookDto.Teacher_uid,
            };
            await booksRepository.CreateAsync(book);
            return CreatedAtAction(nameof(GetByIdAsync), new { id = book.Id }, book);
            //return (item.AsDto());
        }
        [HttpPut("id")]
        public async Task <ActionResult> PutAsync(Guid id,UpdateBookDto updateBookDto)
        {
            var book = await booksRepository.GetAsync(id);
            if (book == null)
            {
                return NotFound();
            }
            book.Uid = updateBookDto.Uid;
            book.Name = updateBookDto.Name;
            book.Description = updateBookDto.Description;
            book.Author = updateBookDto.Author;
            book.Student_uid = updateBookDto.Student_uid;
            book.Teacher_uid = updateBookDto.Teacher_uid;
            await booksRepository.UpdateAsync(book);

            return Ok();
        }
        [HttpDelete("id")]
        public async Task<ActionResult> DeleteAsync(Guid id)
        {
            var book = await booksRepository.GetAsync(id);
            if (book == null)
            {
                return NotFound();
            }
            await booksRepository.RemoveAsync(id);
            return Ok();
        }
    }
}
