﻿using System;

namespace Play.Catalog.Service.BasicApi1
{
    public class PeopleDtos
    {
        public record PeopleDto(Guid Id, string Name, string Room);
        public record CreatePeopleDto(string Name, string Room);
        public record UpdatePeopleDto(string Name, string Room);
    }
}
